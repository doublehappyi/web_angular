/**
 * Created by yishuangxi on 2015/10/28.
 */

var phonecatControllers = angular.module('phonecatControllers', []);

phonecatControllers.controller('PhoneListCtrl', ['$scope', 'Phone', function($scope, Phone){
    //$http.get('phones/phones.json').success(function(data){
    //    $scope.phones = data;
    //});
    $scope.phones = Phone.query();
    $scope.orderProp = 'age';
}]);

phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', 'Phone', function($scope, $routeParams, Phone){
    var phoneId = $routeParams.phoneId;
    //$http.get('phones/'+phoneId+'.json').success(function(data){
    //    $scope.phone = data;
    //    //设置默认的主要图片
    //    $scope.mainImageUrl = data.images[0]
    //});
    $scope.phone = Phone.get({phoneId:phoneId}, function(phone){
        $scope.mainImageUrl = phone.images[0];
    });
    $scope.setImage = function(imageUrl){
        $scope.mainImageUrl = imageUrl;
    }
}]);
