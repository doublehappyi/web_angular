/**
 * Created by yishuangxi on 2015/10/28.
 */
var phonecatServices = angular.module('phonecatServices', ['ngResource']);

phonecatServices.factory('Phone', ['$resource', function($resource){
    return $resource('phones/:phoneId.json', {}, {
        query:{
            method:'GET',
            params:{
                phoneId:'phones'
            },
            isArray:true
        }
    });
}]);