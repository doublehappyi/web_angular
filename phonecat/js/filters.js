/**
 * Created by yishuangxi on 2015/10/28.
 */
angular.module('phonecatFilters', []).filter('checkmark', function(){
    return function(input){
        return input ? '\u2713' : '\u2718';
    }
});